import React from 'react'
import ReactDOM from 'react-dom'

import styles from './styles.module.css'

const App = React.createClass({
  render: function() {
    return (
      <div className={styles.wrapper}>
        <h1>
          Hello!
        </h1>
      </div>
    )
  }
});

module.exports = App;