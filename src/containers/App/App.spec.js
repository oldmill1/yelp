import React from 'react'
import { expect } from 'chai'
import { shallow } from 'enzyme'

import App from './App'
import styles from './styles.module.css'

describe('<App />', function () {
  it('renders as a div', () => {
    const wrapper = shallow(<App />);
    //wrapper.debug();
    expect(wrapper.type()).to.eql('div');
  })
});